## Remi

Pale skin. Average height. Jagged cut hair, as if with a knife. Stern face. Doesn't look very shocked.

She was training to be a Dark Clerk. They worked for Vetenari. They are as good with paper as they are with a blade. Now they're all gone. But because her training isn't finished, she can't get her license to be an assassin from the Assassins' Guild.
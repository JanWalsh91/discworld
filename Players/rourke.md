# Rourke

## Characteristics

- doesn't like to carry a lot of stuff on him
- prefers to use his "natural gifts" rather than weapons to eviscerate his enemies 

### Personality Traits

- Loyal and reliable
- Friendly
- Rash
- Hands on personality, especially with hot men...
- Has a complicated relationship with magic. Love/Hate. Is often suspicious.

### Ideals

- Nature. The natural world is more important than all the constructs of civilization. (Neutral)
	- => I value Nature. I will fight against the desecration of nature.
-  Might. If I become strong, I can take what I want--what I deserve. (Evil)
	- => I value Power Strength and Dominance. I fear being weak in face of what he wants.
- Friendship. Material goods come and go. Bonds of friendship last forever. (Good)
	- => I value his Friends and Loved ones. I fear loneliness.
- Tradition. The ancient traditions of worship and sacrifice must be preserved and upheld. (Lawful) 
	- => Ancient traditions must exist for a reason. I don't understand it, but let's not anger the gods.

### Flaws

- Attractive men are a distraction, and a need.
- I am quick to trust people, especially if they promise to get me what I want.
- I harbor dark bloodthirsty thoughts about those who abuse their power.

## Physical Description

- Ripped bod
- Strong musky manly smells that attract other musky men
- Tattoos
	- From wild werewolf clan. Clan symbology ?
		- Geometric shapes cover his arms and shoulders. He has a turtle on his shoulder.
		- https://scene360.com/wp-content/uploads/2016/06/geometric-tattoos-05.jpg
		- https://stylesatlife.com/wp-content/uploads/2018/06/Mayan-Tattoo-2.jpg
	- Tattoos from the Fairyland
		- Abstract-seeming colorful tattoos cover his back. They are actually vaguely reminiscent of plants and creatures from Faerieland
		- https://www.tattoosandtattoos.com/wp-content/uploads/2020/01/minimalist-shoulder-tattoo.jpg
		- The colors of the Faerie tattoos are visible through the wolf's fur, but burst with color when his Wild Magic is triggered.
- Short dark-brown hair
- Short but well groomed facial hair
- Green eyes

## Personal Relationships

- Zilvinas Gudel (Zilv) - Mate
- Nev Klutz
- Ravens' Bloom
- His unborn child
- Zilv's family:
	- Father: Destrasa Gudel (Yellowfang)
	- Mother: Amalla Gudel (Silvertail)
	- Sister: Janither Gudel (Moonlight)

## Bio (Written History)

- Rourke was born in wild werewolf clan, but he doesn't remember anything about his family or his clan.
- Age 12: Playing around with my friends, I found magical stone circles. I carelessly tumbled through them and ended up in Fairyland.
- I don't remember much from Fairyland, but over time, I remembered a few details
	- The stone gateways are linked with Fairyland. They are probably a way to communicate to the other side.
	- There was an overall good feeling of fun, happiness and carelessness. I played with younger humanoids who treated me like a pet, ... and I enjoyed it. They were not hostile. 
	- Babies aren't born there. New life just kinda turns up.
	- Strong feeling remain around a figure that I probably played with a lot. But I don't remember exactly who or what she was.
- Though he doesn't remember, Rourke wanted to leave but couldn't, and the beings there weren't too fond of him wanting to leave. One of them offered to help him, but for the price of the memories of his family, which he accepted.
- Age 17: I returned to the realverse, years later, as time passed more slowly in Fairyland. My memories of my early youth were gone, and those from Fairyland were scattered and vague. The only thing left on me was a black onyx pendant, which I wore around my neck. My lack of identity and self drove me mad, and my rage triggered bursts of uncontrollable magic.
- For almost a year, I was lost and without a pack and roamed the land virtually alone, save for the occasional deer.
- Age 18: I found my way to Uberwald, working menial jobs to get some coin. It was easier now to live among others as I was able to better control the Wild magic.
- I met Zilv Gudan for a holiday event. He a rough gem from snotty family, and we quickly fell in love.
- Zilv's family was enraged at first. Then they though it was just going to be some summer fling. But even then, I was never heartily accepted by Zilv's family.
- Though I was mistreated by the nobles, they eventually, though reluctantly, tolerated me for the sake of their son.
- A few years later, we realized we wanted our own biological children. We researched desperately. We read ancient tomes. We talked to old hags and wizards. It wasn't possible, they said.
- Age 25: Coincidentally, I rediscovered the onyx pendant that I left in a drawer years ago. Upon its touch, feelings and parts of memories washed over me. I remembered her name: Ravens' Bloom. She was one of my friends from Fairyland and the one that left me with this stone. We were so happy to hear from each other and talk to each other. I remembered more fond memories of us in Fairyland. "I can help you get what you want," I heard. I took Zilv to the stones where we could better talk with Ravens' Bloom. We accepted her help and gave her, to start, a flask of our wolf saliva from each of us. We were to help her fetch the necessary ingredients. But we would have to go our separate ways. Knowing that we would be parted for long, we eagerly and quickly left for our missions, guided by Ravens' Bloom. 
- Ravens' Bloom talks with me through the stone and and in my dreams. She guides me to what I thought were mundane items.
	- A wooden spoon steeped in Borogravian honey.
	- A feather from a female eagle in the Rammerock mountains.
	- Blades of grass that grew on the bank of the Smarl River.
- To exchange items physically, she will require it to be through the stone circles
- If she leaves a physical note, I would know that it is important.
- The ritual has started and the soul is born. I cannot stop it else I fear my child's soul will die.
- I take it upon myself to fulfill their requests for the sake of our child (reason for being an adventurer)
- My next mission takes me to Ankh-Morpork, where I must retrieve some ancient book. Wait no, just a page from it. Hold on, just a corner of it.
- Nev and I met in the Broken Drum tavern, and discovered our shared interest in Ronald Rust.

Their first target, the Colossus of Morpork. Nev lured Mr. Scant into a dark alleyway with the promise of a new monument. Everything went wrong when he felt Rourke’s big paws rummaging in his pocket. Nev used his magical prowess to put Mr Scant to sleep. Eager to impress Rourke, who was disappointed by the lack of violence in rendering Mr Scant unconscious, he threw a rock at his head and they scarpered.

The second target, bread baked by B’hrian Bloodaxe himself – the throwing scone. This was a stepping scone heist before the grand prize. They ground down and baked the previously acquired colossus into imitation bread and threw a dress into the river. A clacks signal was sent to alert the guards of a woman in all-white clothing that had fallen into the river and needed rescuing. Using this distraction, they snuck into the museum, swapped the baked colossus bread with the scone of stone and escaped.

Filled with confidence, they were finally ready for the grand target: the bottom right corner of page 6 of the Octavo owned by Ronald Rust himself which also opened up the perfect opportunity to frame Mr. Gryle by sending a distress signal from Rust’s own personal clack tower.

They trawled through the underground city and broke through Rust’s basement walls with a well placed toss of the throwing scone. However, as luck would betray them, there was no bottom right corner of page 6 of the Octavo owned by Ronald Rust. Instead a letter address to both of them, demanding that they join a group of investigators in Ankh-Morpork…

- Age 26: Session 1

## Goals

### Short Term Goal:

- Complete heist/crime/theft with Pyroo
- My thirst for the hunt excites me. I like a challenge.

### Mid Term Goal: 

- Obtain next artifact assigned by Patron: bottom right corner of page 6 of the Octavo

### Long Term Goal: 

- Complete all requirements of Fey and be forever reunited with Rourke and child
- Protect my child and Rourke above all else

## Secrets

(pick a couple or more things that your character might not want to divulge, or dirty past actions, you know the drill)
- I stumbled into FaerieLand as a young werewolf
- I returned yet again to make a pact with Ravens' Bloom.

## Strong Ties / Bonds

(Pick a few things or people that your character has strong connections to, or strong emotions about)
- My LifeMate, Zilv (Zilvinas Gudel)
- Ravens' Bloom, our "Patron"

## Notable Information

- What is Rourke's relationship with Zilv like?
	- Rourke and Zilv are deeply in love.
- How does Rourke feel about being so far away from Zilv?
	- They have not seen each other for one year and Rourke misses Zilv deeply. But he is filled with the hope that they will one day see each other again. The Onyx pendant also allows connects him to the soul of their child. His life in Ankh Morpork has been full of distractions. Planning heists and enjoying the "nightlife". Rourke and ilv enjoy an open relationship, but remain fully committed to each other.
- What is his relationship with Zilv's family like?
	- Rourke has once tried to please his family, for Zilv's sake, but has given up long ago. He is not a fan of the werewolf nobility. They're so stuck-up and obsessed with bloodlines. Zilv's love has helped Rourke surmount the shame. The nobles called him a mutt, wild, uncivilized, worthless. 
- How does Rourke feel about Ravens' Bloom?
	- Rourke is eager to please Ravens' Bloom and fulfill her demands. His optimism and love for his child has blinded him to think critically about the tasks set before him. 
- What common habits does Rourke have?
	- Has a habit of prowling about in the evening and hooking up.
	- He sleeps in in the morning.
	- He has strong handshakes and is very friendly.


- What are some of Rourke's common phrases?
	- "elephant inside a china shop" - clumsy
	- "jump out of your pants" - overexcited
	- "Counting the crows", "clapping your ears"- inattentive
	- "make an elephant out of a fly" - exaggerate 
	- "Remember, my friends"
	- "..., my friends"

## Pick Up Lines

- Is that a wand of fireballs in your pocket, or are you just happy to see me?
- Is that a rod of reserrection in your pocket, or are you just happy to see me?
- I hope chastity isn't one of the oaths you took.
- I may human on the streets, but I'm a wolf in the sheets.
- Are you a gelatinous cube? Because I think this encounter is gonna end with me inside you.
- I hope you're proficient in animal handling 'cos I'm a beast in bed.
- I see charisma wasn't your dump stat.
- Are you a necromancer, 'cos you're sure making my bone rise.
- Why don't you come back to my place and I'll show you my immovable rod?
- Why don't you come back to my place and I'll show you my rod of reserrection?
- My favorite terrain is your underdark.
- Call me a devil 'cos you've got me horny.
- I hope you're vulnerable to piercing damage.
- Are you proficient in marital weapons? 'Cos I'd love for you to get your hands on my greatsword.
- You have any 5th level spell slots left? 'Cos you could dominate this person.
- Are you a mimic? 'Cos this chest looks too good to be true.
- You should multiclass into cleric, 'cos I want you on your knees.
- Hope you have good animal handling, 'cos I'm a beast in bed.
- You know, orcs aren't the only ones with Relentless Endurance.
- I've got a necklace of fireballs that'll make your insides explode.
- I'm not a wizard, but I can cast Enlarge.
- Is that a mirror in your pants? 'Cos I see myself in them.
- Nice outfit. I want it crumpled up on my bedroom floor.

## Stats

_ | STR | DEX | CON | INT | WIS | CHA | Max HP
-|-|-|-|-|-|-|-
Stats Roll         | 16     | 15     | 16     | 11     | 11     | 11
Werewolf           | 17(+1) | 16(+1) | 16     | 11     | 12(+1) | 11     |
Barbarian(L1)      | 17     | 16     | 16     | 11     | 12     | 11     | 15(12+3)
Barbarian(L2)      | 17     | 16     | 16     | 11     | 12     | 11     | 25(15+7+3)
Barbarian(L3)      | 17     | 16     | 16     | 11     | 12     | 11     | 35(25+7+3)
Barbarian(L4)(ASI) | 17     | 16     | 18(+2) | 11     | 12     | 11     | 49(35+7+4+3)
Barbarian(L5)      | 17     | 16     | 18     | 11     | 12     | 11     | 60(49+7+4)
Barbarian(L6)      | 17     | 16     | 18     | 11     | 12     | 11     | 71(60+7+4)
Barbarian(L7)      | 17     | 16     | 18     | 11     | 12     | 11     | 82(71+7+4)
Barbarian(L8)(ASI) | 17     | 16     | 20(+2) | 11     | 12     | 11     | 101(82+7+5+7)
Barbarian(L9)      | 17     | 16     | 20     | 11     | 12     | 11     | 113(101+7+5)
Barbarian(L10)     | 17     | 16     | 20     | 11     | 12     | 11     | 125(113+7+5)
Barbarian(L11)     |
Barbarian(L12)(ASI)|
Barbarian(L13)     |
Barbarian(L14)     |
Barbarian(L15)     |
Barbarian(L16)(ASI)|

## Detailed Progression

- Level 1
	- **Barbarian**
		- Proficiencies
			- Armor
				- Light armor
				- Medium armor
				- Shields
			- Weapons
				- Simple Weapons
				- Martial Weapons
			- Skills
				- Athletics
				- Intimidation
		- Equipment
			- Greatsword
			- Explorer's Pack
		- Abilities
			- Rage
				- Can rage as a bonus action
				- You have advantage on Strength checks and Strength saving throws.
				- When you make a melee weapon attack using Strength, you gain a bonus to the damage roll that increases as you gain levels as a barbarian, as shown in the Rage Damage column of the Barbarian table.
					- At level 5: +2
				- You have resistance to bludgeoning, piercing, and slashing damage.
				- Ends if turn ends without taking dmg or attacked a hostile creature
					- At level 5, can rage 3 times per long rest
			- Unarmored Defense
				- When you are not wearing armor, you AC is 10 + DEX + CON
	- **Werewolf**
		- Proficiencies
			- Skills
				- Perception
				- Survival
		- Abilities
			- Werewolf
				- You can use your action to turn into a werewolf. You drop all of your equipment and gain the stats and attacks of the wolf described in the Errata of this handout. You retain your original health points. Damage does not force you to turn back. You can shift back as an action, though, if you are wearing or in contact with silver you cannot shift in or out of this form.
			- Darkvision
				- You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. You can't discern colors in darkness.
		- Vulnerabilities
			- damage from silvered weapons
		- Languages
			- Common
			- Uberwaldean
	- **Background: FaerieTouched**
		- Skills
			- Insight
			- Stealth
		- Tool
			- Herbalism Kit
		- Languages
			- Feegle
			- Faerie (Very Basic)
		- Equipment:
			- pouch with 10gp
			- traveler's clothes + tearaway pants
			- 1 potion of healing
			- Pan Flute
		- Feature: Faerie Touched
			- In your time living amongst Faeries and adapting to their ways in order to survive, you have developed a strong awareness of things affected by Faeries or FaeryLand. Items or creatures that have been in contact with Faeries can stand out to you. You occasionally have flashbacks to your time in FaerieLand You have some insight into True Faerie language, and can attempt to decipher it. 
	- Feat
		- Mage Slayer
			- When a creature within 5 feet of you casts a spell, you can use your reaction to make a melee weapon attack against that creature
			- When you damage a creature that is concentrating on a spell, that creature has disadvantage on the saving throw it makes to maintain its concentration
			- You have advantage on saving throws against spells cast by creatures within 5 feet of you
		- (Other options: Charger/Athlete/Grappler/Great Weapon Master/Lucky/Tavern Brawler/Tough/Super Senses)
- Level 2
	- **Barbarian**
		- Abilities
			- Reckless Attack
				- Gives you advantage on melee weapon attack rolls using Strength during this turn, but attack rolls against you have advantage until your next turn.
			- Danger Sense
				- You have advantage on Dexterity saving throws against effects that you can see, such as traps and spells. To gain this benefit, you can't be blinded, deafened, or incapacitated.
- Level 3
	- **Barbarian**
		- Abilities
			- Primal Path: [Path of the Wild Soul](https://media.wizards.com/2019/dnd/downloads/UA-WildAstral.pdf)
			- Lingering Magic
				- You can cast Detect Magic without cost. CON is spellcasting ability. You faintly glow a color corresponding to the school of magic you detect (you choose the colors). You can do so CON mod per long rest.
			- Wild Surge
				- Magic erupts from you when you rage. When you rage, roll on the Wild Surge table (d8). If the wild surge requires a saving throw, the DC equals 8 + your proficiency bonus + your Constitution modifier: 15 (8+3+4)  
- Level 4
	- **Barbarian**
		- ASI (+2 CON) 
- Level 5
	- **Barbarian**
		- Abilities
			- Extra Attack
			- Fast Movement
				- speed + 10 feet
- Level 6
	- **Barbarian**
		- +1 rage per long rest
		- Magic Reserves
			-  As an action, you can touch a creature and roll a d4. The creature recovers an expended spell slot of a level equal to the number rolled. If the creature you touch can’t recover a  spell slot of that level, the creature instead gains temporary hit points equal to five times the number rolled. You take force damage equal to five times the number rolled.
- Level 7
	- **Barbarian**
		- Feral Instinct
			- Your instincts are so honed that you have advantage on initiative rolls. Additionally, if  ou are surprised at the beginning of combat and aren't incapacitated. You can act normally on your first turn, but only if you enter your rage before doing anything else on that turn.
- Level 8
	- **Barbarian**
		- ASI (+2 CON)
- Level 9
	- **Barbarian**
		- Brutal Critical
			- You can roll one additional weapon damage die when determining the extra damage for a  critical hit with a melee attack. This increases to two additional dice at 13th level and  three additional dice at 17th level.
- Level 10
	- **Barbarian**
		- Arcane Rebuke
			- The magic crackling within your soul lashes out. When a creature forces you to make a saving  throw while you are raging, you can use your reaction to deal 3d6 force damage to that creature.
- Level 11
	- **Barbarian**
		- Relentless Rage
			- Your rage can keep you fighting despite grievous wounds. If you drop to 0 hit points while you’re raging and don’t die outright, you can make a DC 10 Constitution saving throw. If you succeed, you drop to 1 hit point instead. Each time you use this feature after the first, the DC increases by 5. When you finish a short or long rest, the DC resets to 10.
- Level 12
	- **Barbarian**
		- Rage use +1 (total: 5)
		- ASI (+2 STR) (+1 STR / +1 INT)
		- OR
		- Feat
			- Athlete
				- You have undergone extensive physical training to gain the following benefits:
					- Increase your Strength or Dexterity score by 1, to a maximum of 20.
					- When you are prone, standing up uses only 5 feet of your movement.
					- Climbing doesn’t halve your speed.
					- You can make a running long jump or a running high jump after moving only 5 feet on foot, rather than 10 feet.
			- Great Weapon Master
				- You’ve learned to put the weight of a weapon to your advantage, letting its momentum empower your strikes. You gain the following benefits:
					- On your turn, when you score a critical hit with a melee weapon or reduce a creature to 0 hit points with one, you can make one melee weapon attack as a bonus action.
					- Before you make a melee attack with a heavy weapon that you are proficient with, you can choose to take a - 5 penalty to the attack roll. If the attack hits, you add +10 to the attack’s damage.
- Level 13
	- **Barbarian**
		- Brutal Critical
			- Brutal Critical increases to two additional dice.
- Level 14
	- **Barbarian**
		- Chaotic Fury
			- At 14th level, you become a wellspring of wild magic while you are raging. As a bonus action, you can reroll on the Wild Surge table, replacing your current effect with the new one.
    - Magic Reserves
      - Now 1d6
## Zilvanas Gudel

## Destrasa Gudel

Zilv's father

## Amalla Gudel

Zilv's mother

## Janither Gudel

Zilv's sister

## Ravens' Bloom

Rourke's "patron"
- met when he was young in Fairyland
- is helping Rourke and Zilv 

## Mr Scant 

- Nev and Rourke stole the Colossus of Morpork from him

## B'hrian Bloodaxe

Baked the throwing scone that Nev and Rourke stole

## Mr Gryle

- CEO of Pay and Sanctions, from Clacks who.
- Is person who Nev is trying to frame, because he's the one who gave him the fine and refused.

## Lord Ronald Rust

- Hated by Nev because he caused his debt.
- Nev was a witness to a caravan owned by Lord Rust which was attacked by werewolves. Nev sent an unauthorized and expensive distress signal through the Clacks.

## Lord Vetenari

- The Patrician the ruler of Ankh MorPork.
- Good "Tyrant" of Ankh MorPork.
- Has walking cane. Aging and frail.
- Did business with Victor before
- person who owns our house and is evicting us.

## Drumnot

- Lord Vetenari's assistant

## Wilfred

- Large burly man. Kind man. Broken nose and scar.
- works at Broken Drum

## Jana

- Herbologist in Anhk Morpork

## Patricia

- came to ITV NEWS to talk about Rodrick
- lives at 39th short street

## Rodrick

- Rumor from old lady that he creates beholders in his basement
- At 42nd short street
- Talked about a "Dibbler" from whom he got bees

## Borbun

- big beefy cat who fought a crocodile and can survive a fireball

## Wisom Page

He owes a glass shopkeeper 15g.

## Dibbler

- street vendor who sells pork sausages. Stole bees. Was turned into by the Beekeeper. 

## Johnsons

Guards at entrance of palace

## Batterson

Large, rosy cheeks, receptionist at Clockmaker's Guild. Mustache.

## Jeremy Clockson

Perhaps person who made the "bullet". Has very meticulously kept clock shop. All clocks are perfectly in sync. Has a beetle clock. Loud clocks that deal force damage. Behind the counter, super thin, well arranged. Glasses. Mouselike face. Slick hair.

## Folke Odwald

Nev's contact at Thieves' Guild. Has burn mark on his face. Attacked by smith in middle of the day. Was promoted. His clothes are really well kept. New title is Accountancy at the Knife's Edge. He's in charge of punishing the unlicensed. He's the best in the city. A lot of nobles and rich merchants come to him.

## Death

Can be contacted through the death of rats.

## Dr Cruces

Head of the Assassins' guild. Master of poisons. Thin. Aged. 
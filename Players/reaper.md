## Physical Description

Towering golem made of stone rocks and held together by vines and covered in bark, twigs, moss, weed flowers, fungus and mushrooms. Bits of dirt are encrusted in between its golem parts. At first glance, he is obviously some prototype that simply hasn't broken down yet. He has empty eyesockets that function as eyes, ears and mouth. He wields a 8-foot-long sturdy branch with a bulbous mossy stump on top, and a rotting pice of bark he uses as a shield in his other hand.

## Backstory

Created ages ago by a wizard who got too lazy to rip the weeds out of his garden himself, he created iterations of Weed Reaper golems. The wizard died before he was able to perfect his creation, leaving him with a measly prototype. Not fully understanding its original command, Weed Reaper tends all plants, but especially weeds and fungus. Passed on from owner to owner, abandoned or chucked out over and over again, he acquired many useful skills over the ages.

## Manual

### **Farmer Golem v0.4.2**

AKA Weed Reaper 0.4.2

- Farmer and weed cutter golem dubbed "Weed Reaper" version 0.4.2.
- Created at Um by Traewrek Trabalar
- Material: stone
- Age: 50,286 years
- Skills:
  - plow tilling
  - seed sowing
  - artificial pollination
  - weed control
  - crop yield optimization
  - germination acceleration
- Plans for future improvement:
  - bee hive management
  - scarecrow replacement (damn crows!)
  - pest control (damn rats!)
  - livestock artificial insemination
- Known bugs:
  - does not deter weed growth
  - has cultivated a useless obsession with weeds, moss, mushrooms and other plant pests
  - cannot override default command
  - commands reset after long periods of inactive state

### Feedback From Owners:

- Reaper is excellent at identifying herbs and their uses for human application
  - Matilda Wartside, nurse
- Mr Mossy has turned out to be effective at deterring unwanted pests. He'll use any farming tool for the job.
  - Kippy Springfield, beetroot farmer
- Fun Gus's accelerated germination feature is great for getting me some more weed. Dope.
  - Nils Smit
- Weed Reaper is not a reliable farmhand... He constantly needs to be reminded of his current task and keeps relocating weeds *on* the garden instead of exterminating them... I can't even access his Chem. What an embarrassment of a golem. When's the next version available?
  - Ella Reed, royal gardens administrator
- I've amplified his abilities through some magical training which has granted him many skills, and many of which are helpful for acquiring special ingredients for my elixirs.
  - Luciana Crane, swamp resident
- Due to a series of unfortunate accidents caused by "misunderstandings", I advise extreme caution for all subsequent owners.
  - Hilda Crane, swamp resident
- He has proven to be an excellent bodyguard and dungeon delving companion.
  - Gabriel Shade, adventurer

## Habits

Is attracted to patches of plants. Analyzes the ratio of weeds to non-weed plants. If there are too many weeds, it'll pluck weeds and plant it on itself. If there aren't enough weeds, it's remove one from its body and add it to the patch of plants.

## Stats

Rolled: 10, 12, 13, 14, 14, 15

_ | STR | DEX | CON | INT | WIS | CHA | Max HP
-|-|-|-|-|-|-|-
Stats Roll         | 12     | 14     | 14     | 13     | 15     | 10
Golem              | 14(+2) | 14     | 16(+2) | 13     | 15     | 10
Feat: Golem Song   | 14     | 14     | 16     | 13     | 16(+1) | 10
Druid(L1)          | 14     | 14     | 16     | 13     | 16     | 10   | 11(8+3)
Druid(L2)          | 14     | 14     | 16     | 13     | 16     | 10   | 19(11+5+3)
Druid(L3)          | 14     | 14     | 16     | 13     | 16     | 10   | 28(19+5+3)
Druid(L4)(Feat)    | 14     | 14     | 16     | 13     | 17(+1) | 10   | 36(28+5+3)
Druid(L6)          | 14     | 14     | 16     | 13     | 17     | 10   | 44(36+5+3)
Druid(L6)          | 14     | 14     | 16     | 13     | 17     | 10   | 52(44+5+3)
Druid(L7)          | 14     | 14     | 16     | 13     | 17     | 10   | 60(52+5+3)
Druid(L8)(Feat)    | 14     | 14     | 16     | 13     | 18(+1) | 10   | 68(60+5+3)
Druid(L9)          | 14     | 14     | 16     | 13     | 18     | 10   | 76(68+5+3)
Druid(L10)         | 14     | 14     | 16     | 13     | 18     | 10   | 84(76+5+3)


## Detailed Progression

- Level 1
  - **Druid**
    - Proficiencies:
      - Armor:
        - Light, Medium, Shields
      - Weapons:
        - Clubs, Daggers, Darts, Javelins, Maces, Quarterstaffs, Scimitars, Sickles, Slings, Spears
      - Tools:
        - Herbalism Kit
    - Saving Throws:
      - Int, Wis
    - Skills:
      - Perception, Nature
    - Equipment:
      - Wooden Shield (Barkshield)
      - Quarterstaff (Mossy Branch)
      - Leather Armor (natural armor?)
      - Explorer's Pack
      - Druidic Focus (his quarterstaff)
    - Spellcasting
    - Cantrips (4):
      - Produce Flame
      - SHillelelelegah
      - Thorn Whip
      - Poison Spray
    - Ritual Casting
  - **Golem**
    - Size: Large
    - Speed: 30ft
    - Immunities:
      - Disease, Poison, Exhaustion
    - Can sleep for 4 hours to get long rest
    - Vulnerability:
      - Magical Bludgeoning Damage
    - Languages:
      - Common
      - Primordial
  - **Background**
    - Ancient
    - https://www.dndbeyond.com/backgrounds/18247-ancient
    - Proficiencies:
      - History
      - Insight
      - Languages (2):
        - Dwarvish
        - Überwaldean
  - **Feat**
    - Golem Song
- Level 2
  - **Druid**
    - Wild Shape
    - Druid Circle
    - Halo of Spores
      - You are surrounded by invisible, necrotic spores that are harmless until you unleash them on a creature nearby. When a creature you can see moves into a space within 10 feet of you or starts its turn there, you can use your reaction to deal 1d4 necrotic damage to that creature unless it succeeds on a Constitution saving throw against your spell save DC. The necrotic damage increases to 1d6 at 6th level, 1d8 at 10th level, and 1d10 at 14th level. 
    - Symbiotic Entity
      - You gain the ability to channel magic into your spores. As an action, you can expend a use of your Wild Shape feature to awaken those spores, rather than transforming into a beast form, and you gain 4 temporary hit points for each level you have in this class. While this feature is active, you gain the following benefits:
        - When you deal your Halo of Spores damage, roll the damage die a second time and add it to the total.
        - Your melee weapon attacks deal an extra ld6 necrotic damage to any target they hit.
      - These benefits last for 10 minutes, until you lose all these temporary hit points, or until you use your Wild Shape again.
- Level 3
- Level 4
  - **Druid**
    - Feat:
      - Telekinetic
- Level 5
- Level 6
  - **Druid**
    - Fungal Infestation
      - Your spores gain the ability to infest a corpse and animate it. If a beast or a humanoid that is Small or Medium dies within 10 feet of you, you can use your reaction to animate it, causing it to stand up immediately with 1 hit point. The creature uses the Zombie stat block in the Monster Manual. It remains animate for 1 hour, after which time it collapses and dies. In combat, the zombie's turn comes immediately after yours. It obeys your mental commands, and the only action it can take is the Attack action, making one melee attack. You can use this feature a number of times equal to your Wisdom modifier (minimum of once), and you regain all expended uses of it when you finish a long rest. 
- Level 7
- Level 8
  - **Druid**
    - Feat
      - Shadow Touched
        - Invisibility
        - Ray of Sickness
- Level 9
- Level 10
  - **Druid**
    - Spreading Spores
      - You gain the ability to seed an area with deadly spores. As a bonus action while your Symbiotic Entity feature is active, you can hurl spores up to 30 feet away, where they swirl in a 10-foot cube for 1 minute. The spores disappear early if you use this feature again, if you dismiss them as a bonus action, or if your Symbiotic Entity feature is no longer active. Whenever a creature moves into the cube or starts its turn there, that creature takes your Halo of Spores damage, unless the creature succeeds on a Constitution saving throw against your spell save DC. A creature can take this damage no more than once per turn. While the cube of spores persists, you can't use your Halo of Spores reaction.



## Strategies

- reaction to use Halo of Spores to damage enemies who walk into it
- Symbiotic Entity to
  - gain TP HP 
  - double Halo of Spores dmg
  - melee attacks to extra 1d6
- if someone dead close to you, Fungal Infestation to reanimate them
- Spreading Spores to send spores far away



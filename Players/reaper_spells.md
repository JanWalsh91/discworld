# 1

- Animal Friendship
- Charm Person
- Create or Destroy Water
- Cure Wounds
- Detect Magic
- Detect Poison and Disease
- Entangle
- Faerie Fire
- Fog Cloud
- Goodberry
- Healing Word
- Jump
- Longstrider
- Purify Food and Drink
- Speak with Animals
- Thunderwave
---
- Protection From Evil and Good

# 2

- Animal Messenger
- Barkskin
- Beast Sense
- Darkvision
- Enhance Ability
- Find Traps
- Flame Blade
- Flaming Sphere
- Gust of Wind
- Heat Metal
- Hold Person
- Lesser Restoration
- Locate Animals or Plants
- Locate Object
- Moonbeam
- Pass without a Trace
- Protection from Poison
- Spike Growth
---
- Augury (ritual)
- Continual Flame
- Enlarge/Reduce
- Summon Beast

# 3

- Call Lightning
- Conjure Animals
- Daylight
- Dispel Magic
- Feign Death
- Meld into Stone
- Plant Growth
- Protection from Energy
- Sleet Storm
- Speak with Plants
- Water Breathing
- Water Walk
- Wind Wall
---
- Aura of Vitality
- Elemental Weapon
- Revivify
- Summon Fey
  
# 4

- Blight
- Confusion
- Conjure Minor Elementals
- Conjure Woodland Beings
- Control Water
- Dominate Beast
- Freedom of Movement
- Giant Insect
- Grasping Vine
- Hallucinatory Terrain
- Ice Storm
- Locate Creature
- Polymorph
- Stone Shape
- Stoneskin
- Wall of Fire
---
- Divination (ritual)
- Fire Shield
- Summon Elemental

# 5

- Antilife Shell
- Awaken
- Commune with Nature
- Conjure Elemental
- Contagion
- Geas
- Greater Restoration
- Insect Plague
- Mass Cure Wounds
- Planar Binding
- Reincarnate
- Scrying
- Tree Stride
- Wall of Stone
---
- Cone of Cold


TO ADD:
- Summon Elemental
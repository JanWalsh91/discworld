Barbarian - Path of the Wild Soul
Werewolf

- Made pact with entity to be able to have child with mate. Gaybies.

Attributes:
- Loyal and reliable
- Great friend
- Rash
- Hands on personality, especially with hot men...
- has a complicated relationship with magic. Love/Hate. Always suspicious.
- Strong musky manly smells
- spends a lot of time as wolf and often prefers it to his human form. 
- doesn't like to carry a lot of stuff on him
- prefers to use his natural gifts to eviscerate his enemies rather than weapons


## History in youth

- participated in battles / war with clan? ... too young ... would not remember ...


## Why be an adventurer?

- I must obey the entity I made a contract with in order to protect the soul of my child and to hope to see him one day in physical form and raise him with Zilv.
- As a loyal werewolf, I value this responsibility.
- My thirst for the hunt keeps me going day to day. (All forms of the hunt ...)


## Ideas for relationship with "Patron"

- She is always on my shoulder, listening to my every move and occasionally interjecting to tell me what to do, or where to go. I reluctantly obey.
- He pops up every now every now and then, as a miniature of himself on my shoulder and yells into my ears telling me to do things. What sucks is that I'm the only one who hears this and I'd look crazy if people catch me muttering to myself.
- She leaves intricate notes with orders, clues, suggestions, or random messages. But otherwise doesn't show up in person.
- The patron want me to collect a series of items and send them to him. They range from mundane objects like tools or food, to rare artifacts or books. 
- Who is this Patron?
	- Old friend that I have vague memories of
- => She communicates with me through dreams and telepathy
- To exchange items physically it will be through the stone circles
- If she leaves a physical note, i would know that it is important.


## Ideas for Special Terms of the Pact

- When directed, you must take immediate action against a specific enemy of your patron
- Your pact tests your willpower; you are required to abstain from alcohol and other intoxicants
- At least once a day, you must inscribe or carve your patron's name or symbol on the wall of a building
- You must occasionally conduct bizarre rituals to maintain your pact
- When you use an eldtritch invocation, you must speak your patron's name aloud or risk incurring its displeasure

- Do I know specifically when the terms will be done? Or was I dumb enough to not read the fine print? 


## Marks

- I got tattoos from my time in Fairyland that Tattoos cover much of my human body
	- TODO: describe tattoos
- An additional tattoo (do fairies lie tattoos??) was placed on me for the pact. It will disappear when the tasks are complete.


## Technical Questions for WereBear

1. What happens to my equipment and clothing when I go in and out of wolf form?
  - Does some equipment stay on me? Ex, pendant? Piercing? Bracer?
		- Case by case. May look for items that a werewolf could wear.
		- Ex: backpack ok
		- Collar, with bezels.
2. Are there factions of fairies?
	- fairies are from one parasite universe. Fairyland. There are many kinds of creatures and strange things. Some are neutral. Some have grudges with each other. Esp Nac Max Feegle. Not a power thing, but show off. Each universe has tides. When tides come in, links with real universe is weaker. Fairyland has a weak barrier. Stone circles are gateway to Fairyland. All parasite universes have some gateway.
3. Confirm how to calculate AC in wolf form? Basically the same, right?
	1. Unarmored Defense: 10 + 3 (DEX) + 4 (CON): 17
	2. Wolf: 10 + 3 (DEX) + 4 (CON): 17
4. Do I get Barbarian features in wolf form?
  1. Rage
  2. Reckless Attack
  3. Danger Sense
  4. Extra Attack
  5. Fast Movement. Wolf speed will be reduced to 40.
  6. ...
  7. GET ALL FEATURES
5. Could I switch out the werewolf's Nature skill for Survival?
6. What kinds of creatures in Fairyland could I have made these pacts with?
7. What remains of my memory as a young werewolf? What replaces my memories of my family? I am aware that there is a gap in my memories?
8. Create/Select Icons on roll20 for Wild Surge status effects

Item: smooth black rock with hole. I wear as a pendant. Onyx black that doesn't reflect light.

## Work with Pyroo's character:

- Hack and Slash plan to commit some crime
- We plan to steal something and ... Frame them?
- ...from Henrietta Blackesley
- we commit a couple of heists to practice for a big heist
- I need to obtain a special item, and he needs to frame somebody for the crime
- The heist has not yet been done by session 1. Can do both. Heist may not be session 1 if we haven't done it yet.

- technicalities of committing crimes in Ankh-Morprk
	- do we have a liscence?

Stole the Colossus of Anhk-Morpork; the smallest statue of the city. Pocket-sized statue. Belonged to Mc Scant. We lure him into a dark alleyway by taunting him with new undiscovered mini-monuments. Nev distracts him with mage hand and I accidentally bump into him as I was trying to reach into his pocket. He turns around and sees me, Nev sees him, so he puts him to sleep. Look disappointed. Throws rock and we scatter.


We baked the Colossus into piece of bread and used it to replace the Throwing Stone of B'hrian Blowaxe in the Museum.


Nev distracted guards through Clack by alerting them that a HEnrietta Blackesley fell into the River and needs to be rescued by valiant men. It was a hot summer and she was wearing all white. We throw a white dress in the river. Guards went looking for her.

We stole a prime example of a dwarven battle bread: a Throwing Scone.
Dwarves see bread as part of war. It is lethal. The bread of Brian Blowaxe. We stole it from a museum. 
Went much better than the first one.


We got the wrong information.

The item we were looking for: a corner of the Octavo. Magical Book said to hold 8 spells that created the world. A bit of the corner of the page of the book. With like 2 letters. Page 6. Held in private collection by a nobleman called Ronald Rust. He's know to be snobby ex military man. Wages wars travelled world. Lives in Anhk Morpork. Last estates outside, but we broke into his estate in Ankh-Morpork. In his basement. We could cross the entire city without being above ground. We wanted to use their personal clack.


We went underground. 


We staked him out to know of his patterns. So we snuck into his basement by crawling through the underground city maze and broke through walls with the Throwing Scone. It was too easy. The piece of the page of the book was not there. But there was a note. 


"Well done: I have a job opportunity for you. The item you're looking for... was never here and I could help you find it."







If we do it before session 1, I did not get the item that I got.
Otherwise, will be an item requested by Patron.

The city Watch deals with murder. Handled by officer Saumel Vimes.

If you steal stuff and get caught, it'll be the thieves' guild that come after you.



## Relationship Pyroo's character:

- Nev Klutz is his name 20 DEX
- best buddy in addition to partner in crime?
## 2020.07.19

Dreams: darkness, not unfamiliar. Lying in bed, wavering in unseen waters around you. No sound of water. On chest, feel pulsing warmth of onyx ring. Bluing rings seeps in from peripheral. Ravens' Bloom comes and talks to me. Tells to bring back the item. I feel the pulsating of onyx ring. 

Moist damp weather. Not too warm.

Teem meeting.

Note may be from watcher.

Clack is 5 silver per "letter".

Go to glassmaker shop.
5th Elephant is a hint from shop keeper.
The marble is really heavy. Inches wide.

Got a large 500 pound marble.

Go to Rodrick's house. Normal looking 3 storey house. Ground-level has shifted. Filthy window. Balcony on upper floor.

It was a BEE-holder.

We eat at Patricia's. She lives alone.

The apiary (north of city) have missing bees.

We go to apiary (half of us).

10 minutes away: much better weather. Warm Spring. Man in orange robes sweeping dirt in the trees. Pilling dirt on top of dirt.

There's a hut where the bee keeper lives. Man crouched with lasso behind a fence.

We catch the bee thief.

First few letters of each paragraph are wrapped. Probably comes from same person.

Detect magic. See in war room, on bookshelf, you catch an item that was forgotten by original three. Something small, no bigger than playing card between couple of books. Summoning.


## 2020.08.09

Brass body could be worth about 200 gold

The sphere turned out to be a mini gravity field. They turn up randomly from time to time. It was imbued into the ball that Ivan brought home.

When we leave, there's a City Guard guard on a box talking to a crowd. "We still solve the murder of Vetenari." He shows up an image of Tae and Victor.

We go to Clockmaker's Guild. We meet the receptionist. We show him the bullet. "We don't make anything like this in guild". Looks like high quality. Maybe made by Jeremy Clockson, who now runs his own clockstore on the edge of hero street. He doesn't know who made the brass plate with R. Jeremy Clockson has only every made clocks. He's really into it.

Then we go to Thieves' Guild. We see divination magic on the doorway. There's a closed eye on the door. 

Folke Odwald.

We hand him the blackmail note.

Rust has something that Folke wants. Deal. He'll come with us into the Manor. We can steal what we each want.

We go to Jeremy Clockson. The firing mechanism was placed on the table, and he recognized it. He pulls a magnifying glass and analyzes it. He's made similar things. He is impressed at how perfect this work is, though he didn't make this one. On the brass piece, he finds a tiny marking: a blank-faced mask with wires coming out around it.

The firing mechanism is a trigger for a secondary effect in another mechanism.

There's a pharmaceutical smell in the back in an alchemy bottle in the back. Dried frog residue with aqua.

We leave the mechanism for 100gp for him to make the blueprint.

We go to Gimble's. Scent leads us to the store. We wait to the store until the others get here.


Has three sample bottles. 
- Rust free
- Crossbow - that's the one.
- Tubing


Lord Rust made a big purchase from Gimble's:  
Large array of clock by proxy of Mr Butler to Lord Rust.

Leads:
- Meet Folke Odwald and take him up on his offer to "break in" Lord Rusts' mansion.
## 2020.10.25

Elevator looks like it was not part of the original structure.

We come down to a huge underground city. Looks like anhkmorepork. The cats are in front of us. There's a line of clockwork soldiers around the city. The are holes/tunnels. Soldiers going into the tunnels. They are guarding what's in the tunnels. Air is very still. Some geysers in the middle. Scent: oil to lube clockwork. Different formula. The square towers have windows with air coming out. They are like giant vents. This is a replica of the central core of the city. The cats go through the clockwork soldiers. Same insides as the one we found in the house. The Enemy clockwork solider look more like the ones that killed Vetenari. The city's soldiers have blue crystals. The enemy soldiers have purple crystals. 

The Watcher was messing with the timeline. The Auditor makes sure the timelines don't mess up. The Auditor took Death's scythe. Watcher is a big beholder.

We arrive at front gates. Closed doors of Palace. We open it. It's a big empty room. There's a large throne on the left with a figure sitting against the chair facing forwards. Figure connected with pipes and cables. Looks like he was once human. Opposite, a small wooden desk, and Vetenari is behind the desk doing paperwork. "It seems you received by notes." He discovered a plot to replace him with a man names Charlie, who looks just like him. V got a note from the man sitting in the throne in the man behind you. Charlie was the one who actually died. They wanted to use Charlie to rule in V's likeness. He's not sure who got him killed. Down here, V's been learning things. Down here, things work much like up there, except like clockwork. 

Blackmail to Nev and I was not from V.
V did not send us Red.
V did write note in invisible ink.
He does not know Ron.
He wanted ITV to learn about consequences that we might be helpful in dealing with.

Currently, we are not under AMP. A schizm in the timeline. Two disks sent opposite ways. We are at the start of the crack. Two timelines are merging together. Something might be slipping through. The blue and purple crystals are the same, but the purple one is corrupted by wizards from the Dungeon Plane. Some of these purple machines have slipped through. It was the one that killed Charlie.

All of the cats are staring up at Jaffer, who is being raised by some power, the figure. Jaffer is struggling for breath. All the cats are just waiting. Jaffer explodes in a puff of smoke. Transformed into Hobnob.

The Big Man says "Good luck chosen heroes" as the cats leave.

Two years ago, they killed the Watcher. The Watcher had broken a rule. The disk has three enemies. The dungeon dimension and two other ones. They have an order between then, taking turns to inflict themselves upon the disk. The Watched skipped in line, breaking the rule. When we defeated him, the breaking of that rule, it broke the world and time: one where he won, one where we defeated him. With the help of three clever men, we tried to protect what was left of humanity. But they did not do too well. Jeremiah Clockson, Ponder Stibbons, Leonard D'Quirm. Together, they created the first clockwork defenders. We were not able to build anymore since they were forced before ground. BIG GUY is what remains of everything. On his throne, we see the two eyed variant of a makers mark. Below his shoulder, graduation tattoo from assassins guild, specializing in poisons. 

Stibbons runs the university and is one of the smartest. This city is called Brass More Pork. Big man has no record of Nev Klutz and cannot see him. 

The cats are from our timeline.

We should go to Land of Glod, Klatch, Vortex Planes, Uberwald, Counterweight Continent.

The longer you spent in this time, it's become more and more of a stone. No pulse of communication. Jeremy Clockson, twin of Jeremiah Clockson.

- find the three who made the first guardian
- give Jeremy the crystal, to help find the other crystals
- watch for shadows

We see a vision of the beholder as we exit.

We fight the Auditor.

"Thanks for letting me back in", he said. He was holding Death's sythe.

We level up.

Ravens' Bloom noticed I was gone.

TODO:
- Respond to Raven's Bloom
- Find people:
	- Jeremiah Clockson (twin of Jeremy Clockson, but location unknown)
	- Ponder Stibbons (University)
	- Leonard D'Quirm (location unknown)
- Why does Nev not exist?
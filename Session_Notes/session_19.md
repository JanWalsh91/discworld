## 2021.01.17

We investigate the dwarf. We find money and a signet ring - Carved dwarven faces, and two dwarven signatures. It's an aged signed ring. His blade is dulled, used for show, for ancestral reasons.

We interrogate him. "Nothing in the cave. They wanted to seal stuff in the cave before anyone got in there." He asks if we work for "it as well". He's worrying about a beholder. He came to the surface last week. Giant eye zapping people. Now, everything is normal again. He and his "brother" were destroying clack towers: no clack towers, no clockwork soldiers. "... was right about the great truth". "When you step away from the page." He is forgetting about "three cycles", "a survivor, babbling mad". He did not last long. Human, male, black hair, short. Met this survivor months before he came to this timeline. Molten-heart is his name. We go down to the force throne. Broodmother: big clockwork beasites. There are at least 4 of them. Half breed giant toads. Our of controls "boulders".

Dwarves are originally from Uberwald. They all have a disdain for Ankmorepork.

MoltenHeart doesn't want revenge or to kill us. The caverns are complex. He tells us where the Forge Throne is (first colony headquarter). Iron and Copper can be found to the northwest. Some resources in the Forge Throne. Gold on path that leads North then East. Adamantine in veins, in North, near the "long mine". Many gemstone in furthest north east corner. But danger there, they "mined too deep". When he got too close, the "light would blind him". Central east, more Iron. Beware open caverns, and piles of scrap, where the BroodMother live. Settlement is central. Advice: find and follow the river.

Their armor is made of FireIron. Is stronger than iron and has resistance to iron. It's a blend of iron and copper. Three disks with his sigil that need to be inserted into the seal. That'll open the doors. They're made from FireIron.

In his version if the world, "The great eye rose. It rose alone and killed people wantonly, with other "walking eyes". They were fought back from clockwork creations, but then the clockwork was turned against them. So they went underground." A group of wizard in XXXX held out a while.

Look for 29g 14s 14c. I give 14s to Nev. Climber's Kit. 50ft Rope * 2. 6 dried rations.

2914c / 5 = 582c = 5g82c

We go to cave. See a dwarf symbol which means the Long Dark or Long Mine - an abode held by dwarves. We hear the echo of water. Smell dwarves everywhere. Metallic tang, one is acrid. See foot-wide holes a bit every new feet. Lined with hair-like substance. These fibers are very file strands of metal that detect our presence.

We see a big cave. Pond of alge with dwarven body in it. Layed in rest. Burial. Eight feet in the liquid/gel. There's a motion down there and a gurgling.

In another room, we see a room full of mechanical spiders. Vulnerable to bludgeoning or thunder.

We fight a broodmother.

She burroughs in the junk.
## 2020.10.18

Ron the fairy tale. We hear muffled mining from far down below. We walk down long soft stone tunnel. We approach the sounds of the mining. We come to a door. Armored dwarf. He thinks Hammersmith may of sent us. He is bleeding. His armor is heavily dented. Don't go in there until the Dregs clear out. Dregs are leaders to deep down dwarves. Just smell blood sweat. But also rock dust and dirt. "The Summoning Dark finds this place". The door is old, faded. We open door.

In room, wooden floor with crash and some roots. Very old. Dead bodies. No more mining sounds. Some new barrels and provisions. Set up for long term stay. Cracks on floor due to heavy weight that used to be on ground. Like heavy creature's feet. Thick fog inside. Smell of blood, vermin, cats. Fire seems to have more potency in the fog. In a room nearby, there's a big round boulder.

We kill boulder golems.

We kill some abysmal rats.

We see dead dwarves with purple gloomy drools. Smells of Ivan's "life" poison. Two years ago, Ivan Vicotor and Tae went to Ramtops. Looks for one true Glod, King. Was an imposter took them down to mineshaft with purple crystal with psychodelic properties. Sam epurple foam pouring out from Vetenari.

We see cats in fight with Clockwork. After we kill Clockwork, Tae sees a maker's mark. 

Ron appears. Vetenari is down. "Clocks tick different time".
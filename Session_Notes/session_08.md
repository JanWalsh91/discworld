## 2020.09.27

We are in the Scrolltree.

Ivan was injected with a very potent version of a drug he knows.

On contact with the Decotavo, it burst in a blue skull flame, then vanished in a puff of smoke.

Ivan died, but Red talked with Death to convince him to take her instead.

As he hits the ground, Red's body shattered. The shared start tinkling across the floor. They're turning grey and stone-like. They start to stack up around Rourke, and are forming a stone circle. No trace of the book left. Circle is forming a 15 ft circle around the plith. The circle forms. One of the book shelves is pushed aside.

An orange hand come through the bookshelves. A giant ape's hand.

At the same time, a figure appears in stone. Zilv. As he touches it, everything materializes and Rourke disappears.

Nev grabs a pair of scrolls placed on plinths.

Stone circle starts to shatter, becoming dust.

They grab onto the hands of the orange monkey and teleport.

Rourke appears in open snowy field. Familiar. Looks tired. Log cabin behind. We are at the Ramtops. We go to the cabin. Romantic log cabin in the woods.

Ravens' Bloom drinking hot drink. She puts Corner in some magical storage. She says she'll be back in a week, but it'll be just a few seconds for her.

I gave her 
- Corner piece of the Octavo
- Bunch of Grass
- Eagle Feather
- Honeyed Spoon

Zilv's got a couple of grey hairs. He worked for Ravens Bloom and got
- A Gnomish Totem (Small wooden carving)
- Crystal from some Dwarves in the Ramtops


They arrive back. There's the librarian. And Remi. The librarian says it's been a week since they left. The librarians takes them to some autobiographies. Finds one called "Red, Igorina". There is no pen scratching coming from this one. They realize that she sacrificed herself.

The Decotavo is a self contained entity. Only exists in human thoughts, not in other books. It has only existed in L-Space.

Tae takes Red's autobiography.


Ravens' Bloom. Disk is center and outside of the Disk, time passes differently.


We go back to base. There's protests. "Pubs Open 24/7" "No spice no life" "Bread Prices too High" "Taxes too High". They will be protesting later that night.

New leadership, including Lord Rust and a few of the nobles families and a representative from each of the guild. Pubs closed after 11pm. Spicy food and street vendors no longer allowed. Now, tax paying is enforced.


Back at the Unreal Estate. There's a letter on the gravitational marble. We can come collect the device at any time from Clockson.

Nev reads the scrolls from L-space. They're both blanc.

From War Room:
- Illusion: Sleeve of cards.
- Conjuration: Book: A Guide to L-Space

Abjuration from upstairs:
- homeless person "Big man go down". Abjuration coming hit hat. Hat is filled with cigarettes. He's kinda drunk. Has a stringless banjo. 

Ivan and Tae in time met an Auditor that makes time work properly. He didn't like them. And try to murder him.

I tell my backstory.

In morning, we wake up to breakfast smell. I intimidate him. He made us food. It smells like some animal product, but it's not bacon. Called Foul Ron. "Biscuits be looking"

We fight Bourbon and his buddies.

A giant cat face appears and says the patron will be back. "The watcher is a friend, the patron always knows, Borbon will be back"

Watchers are involved in the time stuff. He's a gigantic behold that tried to take over the world. Watcher knew everything about us at all times. He likes cats. So we would be suspicious of cats.

They pop into coins.
53 copper
83 silver (given to Nev)
36 gold

We go to Clockson. But the building next to it, is now a store with gibberish written on top of it. We go in. Seems like the shop inside was there for a long time. Has a lot of random items. So many magical items inside. Short shopkeeper. "Widdle's Oddities".

Squeaky Toy: (70g) +5 To Stealth if I squeak it at the same time.

Shop disappeared behind us.

Leads:
- Why does Zilv have some grey hairs?
- Why does the pendant shock others?
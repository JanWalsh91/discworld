## 2021.03.21

Death appears with clipboard. Rourke is not on Death's list. He does not know where he is. He suggests to talk to the Tooth Fairy, or the tooth fairies. We need to go to stone circles.

I use Golem Song to contact a golem historian who specialized in stone cirlces.

=> Stone Shaper 42
- They are a guardian of a stone circle who are willing to comply with a visit.
- We know where they are. They are a day's travel North-East of Quirm.

Remi needs to kill someone to kill Edward D'Eath.

We go to gentrified farming neighborhood.

Pear tree over one of the hedges. Remi saw a familiar figure at the neighbor house. There's a raised voice conversation. Male and Female voice.

Fair middleaged woman answers door. Been working with her hands. Says an Edward lives at the end the street, but is an old man. Remi is looking for an old man. Her husband arrives with a toddler. That husband looks like Edward, but doesn't seem to be the one she needs to kill. Photos on hallway, they all look like recent folders. He calls himself Jeremy. We leave. We meet her mother who says her husband appears in just a couple of years. Appeared out of nowhere claiming that he was attacked by bandits and has a foggy mind. She want him dead. She gives Remi his ring which is his assassin's ring.

Biggie tries to summon Lu Tze

Back in the city, we see a mini goo monster, from the summoning of Death.
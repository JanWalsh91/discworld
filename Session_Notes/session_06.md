## 2020.08.23

Red has been apprehended and exported out.

Folke tries to lockpick the office. Nev gets through. In the office, there smells one man. A lot of pacing behind the desk. Scents peaks around theses notes. He threw his notes on the floor. You also smell that oil. The same oil that was on the clockwork assassin and the clockwork automaton. The oil trail goes straight in and out of the office. Folk grabs a bunch of papers and leaves.

We head down to the basement. There's the steel door.

There is one "butler" in the basement, and probably 4 more in the rest of the mansion. These butlers are probably clockworks.

The steel door is locked. I communed with Ravens' Bloom, who confirms that I have to go through the steel door.

The automatons have bullets. But the automatons are really dumb.

In the basement, smell Lord Rust, but old and faded. Oil on the butlers. Unknown female scent. Faded perfume. Probably Lady Rust. Last couple days.

In room locked up behind bars:
- corner of page of Octavo
- a quill and an empty ink bottle
- envelope with blanc wax seal
- lump of clay
- desk with books
- steel rod

In the middle: big brass orb. Circular designs on it. Look immobile. Set into a bowl on display.

Taking stuff out of the bars causes the brass orb to ring.

Tae touches the ball. It activates. Ooze and tendrils come out from under. Arms and appendages pop out.

We kill it.

We find:
- The corner of a page of the Octavo
- A vampiric book. Has its own field of gravity. Fiend's journal.
- "Don't use this thing on a moving cart." Steel rod. Immovable rod.
- Lump of clay: appears to be a liquid and solid. Very strong as a solid. Does not separate as a liquid.

Folke find a huge turtle and disappears with it, using a rudimentary teleportation spell.

We dismantle the clockwork beholder.

We gop to clack tower. We see guards down there. With city guards looking for Tae.

We escape through water tunnel. Jeremy Clockson waits for us outside of Mansion. Has been pacing in front of our house for hours. "That thing you gave me is amazing! Do you have more?".

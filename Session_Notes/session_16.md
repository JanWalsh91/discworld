## 2020.12.06

A wizard brings us the Book of Tradition. Must be signed members of the university.

Stibbons is going with Clockson into the dwarf hole.

Long hat. Nev was in Clacktower. People there beleived that he was a longhat. In the messages, he found monthly expenditures towards a hattery for 200g. Two entries:
- "I like to keep my head above the water"
and then
- "He didn't manage to keep his head above water"

We go to Clockson to pick up a sword disguises as a walking stick. There's a code that needs to be enter to sheathe the sword.

We go back to bathhouse. Nev goes to special room with old guys who tell him they will meet him later.

In my private room, I smell a bunch of colors. Smells like normal bathhouse.

Big man takes Nev to questioning room. He measures his head. He asks if he believes in magic. He only accepts "no" as an answer. Nev also says he doesn't do magic.

In a back room, Nev sees an orb. He touches it. It's cold. There's a tingling. Touching the orb drains his spells slots by half. In the next room, there's a monopoly man on a chair, with a magnificent top hat. He speaks of a Greater Path and a Treacherous Lie. He thinks it's a lie that magic holds things together. He things it's a conspiracy manipulated by the wizards. He's very passionate. Magic really is just a trick of eye. He asks Nev to gather components. He knows about our group. The orb negates magic. Magic does have a component that they don't understand that allows them to hide their true intentions. They want to find more orbs. Nev suggests that Lord Rust could be disillusioned.

Detect magic:
- Swelling bowl of all sources of magic. Source is outside of my range.
- To the opposite, there's a blind spot, as if it absorbed magic.

My masseuse's name was George.

I go to the back of the building. Can feel those two.

Remi has vampire friend called Lantern that appears to her in her dreams. He in under some boulders.

I turn a trick.

For next time: think of supplies to buy + methods of travel.
## 2020.11.01

Got out of the cave. Almost fell and died.

Went to return the gold pouch and bought ring of protection. For 50g.

In the city, people are building a barricade around the palace. People are taking all kinds of furniture out from the guild halls. The Assassins all left the guild. None are here anymore. 

The University is not under attack. But the wizards are standing ground. Ridculley, one of the wizards. Ivan killed his cat. Ivan is a "cat murderer" and has a reputation for setting things on fire.

In the morning, Vetenari is here. He bring coins and 4 items. 
- Rope with hue in threading, helps with climbing.
- Wooden bamboo Pole
- Satchel Bag
- Clean glass pale blue gem, lights darkness
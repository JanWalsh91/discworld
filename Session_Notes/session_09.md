## 2020.10.04

We go to Clocksons', but it's closed. In the back, we see an old man sweeping the floor in the corner. He gestures us to come in. We have not seen him before. Wears particular robes, like beggars, harmless religious group. Says that Mr Clockson is waiting for us in the basement. And wishes us good luck.

In basement, candlelight. Square block under shop. Arranged into a madman's crustal. Jeremy is facing a wall with a bunch of papers he's drawn on. They are connected with webbing. Looks like a 3D schematic. He finds our mechanism. He noticed that a central piece was missing from the mechanism. A Frequential Trigger. Something that prevents a clock from being offset. It's a crystal. He needs a crystal. Has a design for a schematic that could replace the horse. Looking for a 	purple crystal that vibrates at 88888888 microfrequencies per second. 8 is known to be the color of magic. And is also soft. Our mechanism has tiny particles of 	purple crystal left behind. He doesn't have his medicine. He hasn't slept of bathed in days. He will be developing a frequency detector for 	purple crystals. 

We go to pharmacy. People seem to be going to partician's palace. At pharmacy. Unshacky hands pharmacist. Clockson had an incident at the guild.

Remi reads the porn mag, confusedly. Clockson is less parkisonsy than usual. He's more focused that usual. He says that this medicine slows his mind. He doesn't want to take it. Someone misplaced his clock and he misplaced his tools inside that person. His medicine is made from frog pills, which are very toxic upon contact. Strong hallucinogenic. We give him his medicine. No magic here. Some residue of magic on the mechanism. Very low level at the end of the pins that would hold this. Feels like raw magic. No smell except for oily smell. 

Remi wanted a lisence. Assassin's guild.

Smells from robes guy:
- old but well kept cloth
- head polish (bald man)
- smells like "last week", also smells like "next week". WTF.

The protest is very thick, right outside of Clocksons'. Undertone of undermining dwarves in the protests. They're complaining that dwarves are mining under the city and into their basements. There are people under the Brass Bridge, camping, unfortunates. There's Ron.

We get into the assassins' guild. There are guards on the roof. Inside, it's kinda like a university. Thick plush carpets. Portraits of people. Remi is guiding us. Upper floor. Remi knocks on Dr Cruces' door. Head of the Assassins' guild. Master of poisons. We enter his office. Remi was training to be a Dark Clerk. Vetenari has been "put aside". She could not enter another school because she lacks the qualifications. Remi's teacher, D'eath, has been a problem to the guild. The Head wants him dead. He's gotten it in his head that he's found the 'true king' and wants the Monarchy. We have to return his signet ring as proof, that he received as a student of the Guild. Reward 200g per person. And Remi gets fast tracked in the Guild.

Rourke checks out a hot assassin on the way out. Meet eyes. Reminds me of Folke.

We arrive in front of the Patrician's Palace. 6 Guards standing behind the gates. Heavy armor and halberds. These guys are different, not Johnson and Johsnon. These are *actual* guards. The guards looks a bit nervous. The gates are being shaken. One guards went back and came back with more guards. There are now 8 guards. They poke through the gates and order to get back. On the balcony, the doors open. Some figures including Lord Rust and other nobles appear and look down. Some are guildmasters.

Went to buy:

8 bottles Glumrot Ale 1.5g

8 bottles Haven's Wine 8g

Hissing sound at front door.
Deck of Cards. Illusion Magic: each card has a distinct image on them. 34 cards in total. Red Dragon, Knight with 4 guards, Giant, Beholder, Assassin, 

If you throw down one of these cards, an illusion of that image appears.

Go to Tallow Street: undead thrift "store". Collection of items on sale.

Armored Wrist Bracers(+AC, 500g) + Jacket(1g)

Ring Fools Gold(+AC, 100g)

Friend, Igor, ows cash. Busy, need to get it for him. Igor who runs the Pub just down the street called Biers. We arrive at Biers. We see Dibbler.

## 2020.12.13

Nev has a flatcap.

Plan: go to Quirm.

Hooked up with Joshua from the bathroom. Left me a clacks number. He seemed nervous but also like he had a lot more to give.

3 days to get to Quirm. 

We go shopping.

We go to post office to inquire about coaches to Quirm.

We take a coach. 5gp each. 4 other people. There's mother and child. 2 other men. Mother and child keeping to themselves. One of the men has a find bowler cap. With two intersecting circles on cap.

First stop: 'village' with two houses called Twoshirts. The inn seems to only get money from the coach service. It's otherwise really old. Frail old barman.

Hunting. Rabbits. Also some small reptile like tortoise.

Biggie McSmalls: on tortoise, Nack Fac Feegle. Tats and amulet with tortoise. Clan tattoos. Lightning struck. Prophecy Doom to befall the disc. Go to nearest tree and make a medallion. We get a tortoise medallion. Achoo, The Great Storm. 

Remi takes a mellow mallow.

Tortoise tattoos don't match. But our tribals swirls in tattoo seem to follow same geometry as the ones on my back.

Disc in ruins.

Religious initiation ceremony: + 1d4 on my next saving throw.

Bringer of Good News.

An explosion in the middle of the night. One of the clacks towers is burning. Maybe someone threw a fireball. We rush there. There are dwarves at the wreckage.

We killed everyone and left no witnesses. Great.

They said they came from the west. They were fireproof. Pretty obvious that they did it. They were removing the machine components of the clack tower. They were not trying to help the people.
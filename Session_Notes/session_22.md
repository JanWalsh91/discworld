## 2021.02.14


We found a page "of the Truth", currently untranslated. We hear broodmothers. A bunch of small minions come out and attack.

Someone rips through the broodmothers. As they kill them, they make a deafening sound. They they talk to u to our head "Let's clean this place up a little bit", as it cleans all the bodies away. The Auditor. "You are not in the right timeline". He summons the shadows. We "Kill" the Auditor. He drops Death's scythe. 

The text on the throne is in dwarvish.

We go back to looting IN ARMORY. Chest with coins and gems. Armor. Silver circlets. Two Hand axes. Leather armor. Jars: three bugs made of clockwork. Types of bug: earwig, firefly, bumblebee.

Forge: 5 Fire Iron ingots 2 pounds each. Recipe on how to forge fire iron, that I learn. It's made of adamantine and iron. 1 adamantine, 2 iron. Armor: fire resistance.

Find two satchels.

I find a Potion Supreme Healing in a satchel.

We each get 19p, 93g. I give my silver to Nev and he gives me 12g.

Two hand axes:
- carved gnarled tree branch

We find the guy who was sweeping the floor at Clocksons. Lu Tze. He's a "friend" of Ron. He cleans up his messes. He's from an Order of Monk. Men in Saphron. "we make sure that history and history and things happen as they should. And you are all in the wrong time!". He knows of the Auditor. "The Auditor does the same work that we do, but in a much more dangerous and reckless way. We lke to let things happen and make sure that they remained happened." "Some things have been bleeding through." "I'm here to help facilitate your journey, just like Foul Old Ron, but he's more messy." "My order can safely travel back and forth as we please across and through time. We ensure that history remains history." "Without me, you will experience time dilution. With me, you can get back at the same time that you entered the caves." "The Truth is forbidden in our order." "Everyone has their gods and they exist if someone believes in them right? Ron is an involuntary god of wanderers and homeless. He is directionless." "The Auditor continues to erase things in the wrong timeline, but things are starting to get blended and reckless." There are multiple Auditors. "I am 1400 or such summers old." "Time is a ball of water in which you can freely move up and down." "Two timelines are like two balls of water coming together"

"Every thread needs to be cut before we can separate the timelines"

Detect Magic:
- hand axe
- bow
- Leather Pouch

We end by mining more adamantine!

We all level up!
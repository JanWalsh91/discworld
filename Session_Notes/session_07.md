## 2020.09.06


Ivan is poisoned. High. The poison is starting to revive him, to realive him. 

Raven's Blooms came to me. Pretended to be Zilv. Said to go see the Wizards for the delivery and to help Ivan.

Ivan has a weird disease. Detect Magic doesn't detect magic on Ivan. The slime on Ivan is profoundly non-magic.

Dungeon Dimension is a parasite dimension. Creatures there long to break into Discworld. He was injected with purple liquid.

Nev tinkers with the clockwork body. He finds a globe in the body, which opens up. Inside, there's a small see-through drained crystal. Looks like it could have been a battery. "Brass Heart".

As we leave, we see two cats in the window in front of us, and then disappear.

We go to sell the brass. For 400g. 

We get to the Unseen University Library.

Archancellor.

We get let in by Ponder Stibbins. Into the feasting hall. Wizards are eating ... too much food. Past that, we go to the library.

The Bowlers do the heavy lifting around the university. 

We get into the Library. Endless shelves. Very big. Bigger that it should be. Signs on bookshelves. "Don't feed the tomes".

Desk made of books. Orangutan. We get a book "Dungeon Dimensions for Dummies". Cure for Ivan is in a book Decotavo. The Orangutan won't take us to it. But he'll show us how to get it. In L-Space: the one space that connects all books to other books. Can take us anywhere for the known to the unknown world if you know how to get there. He leads us to a shelf in a brick wall. He puts Tae's hand through the books and Tae disappears with a "pop". We all go through. We fall sideways. We end up on a floor, in a library of similar nature. Except that this one seems larger and messier.

Fredrick the Lorekeeper. I taste moth knowledge.

Speaks of Archchancellor Malloch who died 300 years ago.

Octavo contains 8 spells that created the universe.

The Decotavo contains 10 spells. Helped created the universe and then added unecessary limbs. Parasite dimensions. 

Time doesn't work the same way here.

Need to get through the Scrolltree. Need both books to get inside. Lore and Law.

I need to put my hand on the Decotavo to open the portal to deliver. Then they will burn. 

I place my hand on the Decotavo. The burns blue, an image of a grinning skull appears, and then disappears. Ravens' Bloom's voice "The stone circle will form shortly".

Death appears on top of Ivan's body. 




Bloom can get harsh or blunt with me.

Remember she asked to burn the Decotavo, and that coming here would help Venom.

Use more insight checks on Ravens' Bloom.



Recap:

- RB's voice when I set my hand on the Decotavo, it burns and disappeared. Contact through the onyx stone. Ravens' Bloom's voice "The stone circle will form shortly".
- Saw RB in the bookstacks. She let me know that she needs to destroy it by me placing my hand on it. She said "it was an evil book".
- At house, in bed at night, she said go to Wizards, who will have something to help Venom and me. She will ask a favor when we get there.
- Helped me finding the Ocavto
- In dreams, she said sharply that I've gotta find the Ocavto.
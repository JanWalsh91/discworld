## 2021.03.07

Ivan lost some coin since he was knocked out last night. 

Gendarme comes to inn and brings Rourke's pendant to Ivan, and says that Rourke has been reported as gone missing by Marie and the family. "Went in the ocean and never came back".

Ivan and Biggie get breakfast.

Remi at assassin's guild. Someone gives her some orders in a sealed letter with the Assassin's sigil. "Hearing is an issue for this guild". Was the top dog. Havlock Jones. He's an expert at "dealing with things personally". He specializes in poisons. Havlock Vetenari was specialized in poisons and concealment. Jones is a fanboy of Vetenari and is impressing the others, he's now in clerical leadership.

Biggie and Ivan start looking for Rourke. A Locate Creature doesn't find him. Ivan suggests summoning Death to find him.

Biggies tries to heal Remi, but she's still deaf. She reads Havlock's Jones's order. Has Lord Downy's signature. He runs the Assassin's Guild. "The contract is still available. The Guild has been temporarily dissolved, but still under contract. They believe that D'eath lives on the outskirts of the city."

They go to down, the world shifts, the city is ruined, blown up. There are met by cats and robots. Some of the them were defending the brass city. The constructs are around the robots. The large one has a voicebox and commands the party that they must report to quarantine and curfew below ground. These robots have a plain orange core. They attack. In the middle of combat, the world shifts back to normal. They damaged things around. They have to pay for damages.

They meet a golem seller. Tries to sell a golem. Cost 3 gold.

We gather follower and summon Death in some backyard. Slime octopus attack. We beat them. Death appears.
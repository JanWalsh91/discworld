## 2021.01.03

Workers are probably from Quirm.

Dwarsves have been underground. Smell of underground dirt + fuel + colone.

We go back to cart.

Dwarves are maybe from Quirm, not work for longhats, were not hired.

We take short rest, then go back to track them.

Takes 30mins to track.

There's a clearing with a pile of rocks and dwarf keeping watch.

Behind there is a camp with 5 dwarves.

We assault the first dwarf. I pin, muffle, and manacle him.

We hear dwarves in the crater. They're all in fireproof armor. They're standing in front of a cave entrance. These are Deep Downers. I kill him. They suspect some noise.

Quirm is France.

I summon an Arignes with my cards, and throw caltrops. I blind them.

The main guy turns into some fire elemental.

He turns into a duck. We put him in a pot and cook him, and he turns back into unconscious dwarf. I manacle him.
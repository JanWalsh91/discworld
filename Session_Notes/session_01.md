## 2020.07.13

Going to Unseen Estate

Man drops off package at Estate. Private Carrier. Comes from Uberwald.

Red came in crate.

"Patty boy".

Victor said that the note is from The Patrician the ruler of Ankh MorPork. "Tyrant" of Ankh MorPork. Lord Vetenari.

Old lady at door. Rumor of man making beholders. Name: Rodrick. Creating beholders in his basement.

We go to the Patrician's Palace. A Dark Clerk leads us in. There are busts of previous kings with names underneath. Specified how the king died. All kings were killed. No natural deaths. Lorenzo the kind was executed. Chubby king. 

Empty room with seats, a mistimed clock. Seems purposefully designed to be an annoyance. Is actually keeping.

Drumknot: Patrician's assistant. 

We come to accuse Vetenari of blackmail. He denies everything.

We go through this other door in order to fill a formal request.

We fall in trap down to sewers.

We go to Broken Drum's basement.

We play cards at Tavern.

Got drunk.

Start bar fight.

Borbun appears at the door of the bar, the big beefy cat.

Rumor from bartender: There's an apiary outside the city, and bees are going missing. These are trained homing bees.

The cat is staring us down as we walk home.

Wiles Jaenbeorc: name of prostitute in bar.

Leads:
- origin of mysterious note
- Rodrick creating beholders in his basement
- Borbun the cat is following us
- haunted clack system


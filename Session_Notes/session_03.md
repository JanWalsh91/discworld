## 2020.08.02

leads:

- note that Rourke and Nev found
- Strange timing of delivery of Red. Unnamed gifter.
- Note: don't believe everything you hear

Victor comes back from bar, person delivers a notice of eviction. 

We go to the Patrician. The guards are not lucid. They seem drunk, or something...

Some homeless person walked by. Maybe he poisoned them. Or just nonsense. 

We head inside. And up to the patricians' office' waiting room. We see the clock ticking normally. Nev touches the clock, and a clap of air and something hits the floor. We go into the office. Vetenari is dead with something having jumped out of his back. A person is dark clothes just darts out of the window. It falls and hits the floor. No Blood. Something in his pockets scatters all over the floor. The person is lying on the cobbles where they landed.

In the office: The Patrician's body: in his chest, there's a small hole. His back is completely burst open. Strange sickly yellow foam dribbling outside of his mouth. No blood. It's a real meat and flesh body. It seems to be him. His walking cane is not here, he usually walks with limp. Red finds a beam of red candlelight coming through hole. There's a room of chairs through the hole. Smell the patrician, his assistant, oil and some fireworks. Oil trail, came into the root, sat in corder, back to front door, heads into streets. We hide behind the bushes and wait until we can leave. We follow the lead, down to cunning artificers, where we lose the scent. I "lock in" the scent to remember it.


The ones who stay behind find a small 'bullet'.

Outside: The body on the ground, there are robes and humanoid pieces of metal. Nothing much salvageable from the body, except for a strange clockwork complex pipe in one arm but not the other. Victor distracts onlookers with performance. Some of them are city watch, who have no authority here. Get in a small argument with Victor. Victor invites audience to interact with 'art'. 

The mechanical humanoid smells of the same oil. 

When we broke into Lord Rust's Manor, we had clockwork in displays, similar. This is high end expensive work. Could be the Clockmaker's Guild.

We found a new thing in upper room. A clatch receiver. In the chair, a half person stuck to the chair. Clack system keeps log of what was sent through in a drum full of rolls of punch cards. The person in the chair in non-responsive. Their eyes look fake. Nev takes the drum, and the body falls forward. The back of his head is made of brass.

The message is one word repeated constantly: Bye. It dates from about half a year. Rich people may have personal clacks. The brass humanoid have same oil smell. Red will perform an autopsy on the body. The body has trouble peeling off the chair. Everything under the skin is brass clockwork. Looks like older model. It's corroded. It's old.

The maker's mark starts with R. Lord Rust?

Detect Magic: it seems like the clockwork piece has absorbed magic. It's made from human skin. 

Leads:
- Clockwork Guild
- Leather workers (oils)

Victor manages to connect with someone? He was 14 when he met Lilian. They clicked. She is the mother of Haiden. Lilian disappeared one day. He could not find her. He has to go to Lancra. Because that's what the voice said.